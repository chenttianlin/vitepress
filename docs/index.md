---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "代码林"
  text: ""
  tagline: 一键CV
  actions:
    - theme: 前端
      text: Markdown Examples
      link: /markdown-examples
    - theme: 后端
      text: Markdown Examples
      link: /markdown-examples
    - theme: 源码
      text: API Examples
      link: /api-examples

features:
  - title: 细致讲解
    details: 丛0开始
  - title: 一键CV
    details: CV
---

