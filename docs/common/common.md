## 类名
```css
/* 推荐 */
.button-primary {
  /* 样式定义 */
}
```

## uniapp 请求
```js
// 1 让页面进入加载状态
// 2  请求
// 3  结果处理
// 4  加入cahch  显示错误
// 5  加入finally 关闭加载状态

// 封装方法
showLoading(title = "加载中···", mask = true) {
    uni.showLoading({
        title: title,
        mask: mask
    })
}

// 1 让页面进入加载状态
// uni.showLoading({title:'加载中'})
this.showLoading("加载中···", true);
//2  请求
方法(参数).then(res => {
  // 3  结果处理
}).catch(err => {
  // 4  加入cahch  显示错误
  //uni.showLoading({title: err.msg,mask: true})
  uni.showToast({
     title: err.msg,
     icon: "none",
     duration: 2000,
  });
}).finally(() => {
  // 5  加入finally 关闭加载状态
  uni.hideLoading();
})


uni.showLoading({title:'加载中'})

.catch(err => {
  uni.showToast({
     title: err.msg,
     icon: "none",
     duration: 2000,
  });
}).finally(() => {
  uni.hideLoading();
})



// 请求中的状态
loading:false,
// 下拉状态
refresh:false,
/**
* 监听用户下拉动作
*/
onPullDownRefresh() {
  this.refresh = true
  this.init()
},

init(){
  this.loading=true
  uni.showLoading({title:'加载中'})
  clientApi.familyList({}).then(res=>{
    this.userList.length=0
    this.userList=res.rows
    this.loading=false
    if (this.refresh) {
      uni.showToast({
        icon:'success',
        title:`刷新成功`,
        duration: 1500
      })
      this.refresh=false
      uni.stopPullDownRefresh()
    }
    !this.refresh && uni.hideLoading()
  })
}

```


## 🎯动态背景图
```js
:style="{backgroundImage: `url(${form.avatarUrl})`}"
:style="{ 'height':isShowMap ? 'calc(100vh - 460rpx)':'calc(100vh)','paddingBottom':`${bottomHeight}px`}"
:class="i===selectItem?'select_item':'unselect_item'"


  :style="{
    color: `${report.indexValue >= 0 && report.indexValue <= 20 ? '#5ADAAF' :
    report.indexValue > 20 && report.indexValue < 40 ? '#73D94D' :
    report.indexValue >= 40 && report.indexValue < 60 ? '#ABDA49' :
    report.indexValue >= 60 && report.indexValue < 80 ? '#FFD742' :
    report.indexValue >= 80 && report.indexValue < 100 ? '#F78434' : '#E72A33'}`
  }"
```


```js
const test = () => {
  addCart({
    b: 22,
    success: (res) => {
      console.log(res, '==2=='); // 正确打印计算结果
    }
  });
};

const addCart = (obj) => {
  let a = obj.b * 2;
  console.log('==1==');
  // 移除对未定义变量 res 的引用
  obj.success(a); // 直接调用回调并传递计算结果
};

```

## colorUi
```tel
750rpx

xs	sm	lg	xl

flex-sub     所占比例 1
flex-twice   所占比例 2
flex-treble  所占比例 3

flex-direction   主轴的方向,坚向排列

justify-start    对齐方式 从行首开始排列
justify-end      对齐方式 从行尾开始排列
justify-center   对齐方式 居中排列
justify-between  对齐方式 均匀排列每个元素，首个元素放置于起点，末尾元素放置于终点
justify-around   对齐方式 均匀排列每个元素，每个元素之间的间隔相等


align-start  对齐方式 元素位于容器的开头
align-end    对齐方式 元素位于容器的结尾
align-center 对齐方式 元素位于容器的中心

position

静态定位	static
相对定位	relative
绝对定位	absolute
固定定位	fixed

深入理解 flex-grow、flex-shrink、flex-basis

flex-grow   数值			瓜分剩余空间  默认是0
flex-shrink  数值			超出了,进行压缩
flex-basis  理解成 宽（width）属性，用法和 width 的一致，只是优先级比 width 更高



.text-xsl  文字大小 60px 用于图标、数字等特大显示
.text-sl   文字大小 40px 用于图标、数字等较大显示
.text-xxl  文字大小 22px 用于金额数字等信息
.text-xl   文字大小 18px 页面大标题，用于结果页等单一信息页
.text-lg   文字大小 16px 页面小标题，首要层级显示内容
.text-df   文字大小 14px 页面默认字号，用于摘要或阅读文本
.text-sm   文字大小 12px 页面辅助信息，次级内容等
.text-xs   文字大小 10px 说明文本，标签文字等关注度低的文字



图标
<view class="cuIcon-pic"></view>
```

## 超出文本

``` css
.truncate-text {
    /*  */
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    line-height:40rpx;
    text-overflow: ellipsis;
    height: 80rpx;
}
    
```



```vue
<template>
  <div>
    <template v-for="(item, index) in options">
      <template v-if="values.includes(item.value)">
        <span
          v-if="(item.raw.listClass == 'default' || item.raw.listClass == '') && (item.raw.cssClass == '' || item.raw.cssClass == null)"
          :key="item.value"
          :index="index"
          :class="item.raw.cssClass"
          >{{ item.label + ' ' }}</span
        >
        <el-tag
          v-else
          :disable-transitions="true"
          :key="item.value"
          :index="index"
          :type="item.raw.listClass == 'primary' ? '' : item.raw.listClass"
          :class="item.raw.cssClass"
        >
          {{ item.label + ' ' }}
        </el-tag>
      </template>
    </template>
    <template v-if="unmatch && showValue">
      {{ unmatchArray | handleArray }}
    </template>
  </div>
</template>

<script>
export default {
  name: "DictTag",
  props: {
    // 包含所有可能的标签选项，每个选项是一个对象，至少包含value和label属性，以及可选的raw属性（后者包含listClass和cssClass等样式信息）。
    options: {
      type: Array,
      default: null,
    },
    // 指定要显示的标签的值。如果是一个数组，则显示数组中所有值对应的标签；如果是单个值，则显示该值对应的标签。
    value: [Number, String, Array],
    // 当未找到匹配的数据时，显示value
    showValue: {
      type: Boolean,
      default: true,
    },
    // 当value是字符串时，用于分割字符串以获取多个值。
    separator: {
      type: String,
      default: ","
    }
  },
  data() {
    return {
      // 用于存储未匹配的项（即value中存在但options中不存在的值）。
      unmatchArray: [], // 记录未匹配的项
    }
  },
  computed: {
    // 根据value生成一个数组，数组中的每个元素都是字符串类型。如果value是数组，则直接映射为字符串数组；如果value是字符串，则按separator分割为字符串数组。
    values() {
      if (this.value === null || typeof this.value === 'undefined' || this.value === '') return []
      return Array.isArray(this.value) ? this.value.map(item => '' + item) : String(this.value).split(this.separator)
    },
    // 判断是否存在未匹配的项，并将这些未匹配的项存储在unmatchArray中。如果存在未匹配的项，则返回true；否则返回false。
    unmatch() {
      this.unmatchArray = []
      // 没有value不显示
      if (this.value === null || typeof this.value === 'undefined' || this.value === '' || this.options.length === 0) return false
      // 传入值为数组
      let unmatch = false // 添加一个标志来判断是否有未匹配项
      this.values.forEach(item => {
        if (!this.options.some(v => v.value === item)) {
          this.unmatchArray.push(item)
          unmatch = true // 如果有未匹配项，将标志设置为true
        }
      })
      return unmatch // 返回标志的值
    },

  },
  filters: {
    // 用于处理unmatchArray，将其转换为一个由空格分隔的字符串。如果unmatchArray为空，则返回空字符串。
    handleArray(array) {
      if (array.length === 0) return '';
      return array.reduce((pre, cur) => {
        return pre + ' ' + cur;
      })
    },
  }
};
</script>
<style scoped>
.el-tag + .el-tag {
  margin-left: 10px;
}
</style>

```



