四、类型声明
使用 : 来对变量或函数形参，进行类型声明：

let a: string // 变量a只能存储字符串
let b: number // 变量b只能存储数值
let c: boolean // 变量c只能存储布尔值
 
a = 'hello'
a = 100 // Type 'number' is not assignable to type 'string'
 
b = 123
b = 'hello' // Type 'string' is not assignable to type 'number'
 
c = true
c = 123 // Type 'number' is not assignable to type 'boolean'
 
// 参数x和y必须是数字，函数返回值也必须是数字
function sum(x: number, y: number):number {
    return x + y
}
 
sum(1, 2)
sum(1, '2') // Argument of type 'string' is not assignable to parameter of type 'number'
sum(1, 2, 3) // Expected 2 arguments, but got 3
sum(1) // Expected 2 arguments, but got 1
在 : 后也可以写字面量类型，不过实际开发中用的不多

let a: 'hello' // a的值只能是字符串'hello'
let b: 100 // b的值只能是数字100
 
a = 'hello world' // Type '"hello world"' is not assignable to type '"hello"'
b = 200 // Type '200' is not assignable to type '100'
五、类型推断
TS 会根据我们的代码，进行类型推导，例如下面代码中的变量 a ，只能存储数字

let a = 100 // TypeScript 会推断出变量 a 的类型是数字
a = false // Type 'boolean' is not assignable to type 'number'
 
let obj = {
    name: 'xiaoming',
    age: 6
}
obj.name = null // Type 'null' is not assignable to type 'string'
obj.age = '8' // Type 'string' is not assignable to type 'number'
但要注意，类型推断不是万能的，面对复杂类型时推断容易出问题，所以最好明确类型声明！

六、类型总览
JavaScript 中的数据类型

string
number
boolean
null
undefined
bigint
symbol
object
备注：其中 object 包含：Array、Function、Date、Error 等......

TypeScript 中的数据类型

上述所有 JavaScript 类型
六个新类型
any
unknown
never
void
tuple
enum
两个用于自定义类型的方式：
type
interface
注意点

在 JavaScript 中的这些内置构造函数：Number、String、Boolean，它们用于创建对应的包装类型，在日常开发时很少使用，在 TypeScript 中也是同理，所以在 TypeScript 中进行类型声明时，通常都是用小写的 number、string、boolean。

例如下面的代码：

let str1: string
str1 = 'hello'
str1 = new String('hello world') // Type 'String' is not assignable to type 'string'. 'string' is a primitive, but 'String' is a wrapper object. Prefer using 'string' when possible.
 
let str2: String
str2 = 'hello' // 不会报错
str2 = new String('hello world') // 不会报错
1.原始类型 VS 包装类型

原始类型：如 number、string、boolean，在 JavaScript 中是简单数据类型，它们在内存中占用空间少，处理速度快。
包装类型：如 Number 对象、String对象、Boolean对象，是复杂类型，在内存中占用更多空间，在日常开发中很少由开发人员创建包装对象。
2.自动装箱：JavaScript 在必要时会自动将原始类型包装成对象，以便调用方法或访问属性。

自动装箱示例代码：

// 原始类型字符串
let str = 'hello'
 
// 当访问 str.length 时，JavaScript 引擎做了以下工作：
let size = (function() {
    // 1.自动装箱：创建一个临时的 String 对象包装原始字符串
    let temStringObject = new String(str)
 
    // 2.访问 String 对象的 length 属性
    let lengthValue = temStringObject.length
 
    // 3.销毁临时对象，返回长度值
    return lengthValue
})()
 
console.log(size) // 5
