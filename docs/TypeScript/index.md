TypeScript（TS）增强了 JavaScript 的静态类型检查能力，提高了代码的可维护性和可读性。通过类型定义，TS帮助开发者在编写阶段发现并修正错误，促进了大型项目中的团队协作和代码质量，在前端开发中的具有重要意义。






<tag></tag>
<alert></alert>

## 一、TS是什么？

- TypeScript 由微软开发，是基于 JavaScript 的一个扩展语言。
- TypeScript 包含 JavaScript 的所有内容，是 JavaScript 的超集。
- TypeScript 增加了静态类型检查、接口、泛型等很多现代开发特性，因此更适合大型项目的开发。
- TypeScript 需要编译为 JavaScript，然后交给浏览器或其他 JavaScript 运行环境执行。
- TypeScript 支持任意浏览器，任意环境，任意系统并且是开源的。

## 二、为什么需要TS？

**今非昔比的 JavaScript（了解）**

- JavaScript 当年诞生时的定位是浏览器脚本语言，用于在网页中嵌入一些简单的逻辑，而且代码量很少。
- 随着时间的推移，JavaScript 变得越来越流行，如今的 JavaScript 已经全栈编程了。
- 现如今的 JavaScript 应用场景比当年丰富的多，代码量也比当年大很多，随便一个 JavaScript 项目的代码量，可以轻松达到几万行，甚至几十万行！
- 然而 JavaScript 当年 "出生简陋"，没考虑到如今的应用场景和代码量，逐渐的就出现了很多困扰。

**JavaScript 中的困扰**
1.不清不楚的数据类型

```js
let welcome = 'hello'
welcome() // TypeError: welcome is not a function
```

2.有漏洞的逻辑

```js
const str = Date.now() % 2 ? '奇数' : '偶数'

if (str !== '奇数') {
  console.log('hello')
} else if (str === '偶数') {
  console.log('world') // 这里永远不会执行，但不会报错
}
```

3.访问不存在的属性

```js
const obj = {
  width: 20,
  height: 10
}

const area = obj.width * obj.heigth // 这里并不会报错

console.log(area) // NaN
```

4.低级的拼写错误

```js
const message = 'hello,world'
message.toUperCase() // 拼写错误但不会提示
```

**【静态类型检查】**

- 在代码运行前进行检查，发现代码的错误或不合理之处，减少运行时异常的出现几率，此种检查叫【静态类型检查】，TypeScript 核心就是【静态类型检查】，简言之就是把运行时的错误前置。
- 同样的功能，TypeScript 的代码量要大于 JavaScript，但由于 TypeScript 的代码结构更加清晰，在后期代码的维护中 TypeScript 却远胜于 JavaScript。

## 三、编译TS

浏览器不能直接运行 TypeScript 代码，需要编译为 JavaScript 再交给浏览器解析器执行。

1.命令行编译

要把 .ts 文件编译为 .js 文件，需要配置 TypeScript 的编译环境，步骤如下：

第一步：创建一个 demo.ts 文件，例如：

```js
const person = {
  name: '小明',
  age: 6
}
console.log(`我叫${person.name}，我今年${person.age}岁了`)
```

第二步：全局安装 TypeScript

```
npm i typescript -g

查看版本号

tsc -v
```

第三步：使用命令编译 .ts 文件

```
tsc demo.ts

依次编译多个.ts文件

tsc demo.ts demo2.ts demo3.ts
```

执行编译命令后，会生成一个 demo.js 文件

```
var person = {
    name: '小明',
    age: 6
};
console.log("\u6211\u53EB".concat(person.name, "\uFF0C\u6211\u4ECA\u5E74").concat(person.age, "\u5C81\u4E86"));
```

如果 tsc 命令不能被识别，管理员权限打开 PowerShell 输入：set-ExecutionPolicy RemoteSigned 按回车。

2.自动化编译

第一步：创建 TypeScript 编译控制文件

```
tsc --init
```

工程中会生成一个 tsconfig.json 配置文件，其中包含很多编译时的配置。
观察发现，默认编译的 JS 版本是 ES7，我们可以手动调整为其他版本。
第二步：监视目录中的 .ts 文件变化

```sh
tsc --watch
```

第三步：小优化，当编译出错时不生成 .js 文件（默认情况下出错也会生成 .js 文件）

```sh
tsc --noEmitOnError --watch
```

备注：也可以修改 tsconfig.json 中的 noEmitOnError 配置

3.不编译运行

如果在 Node 环境，可以直接运行.ts 文件吗？答案是可以的。不过需要全局安装 ts-node，然后就可以直接用 node 运行 .ts文件了。

```sh
npm i -g ts-node

ts-node demo.ts
```

注意：

相同文件夹下的不同 .ts 文件不能有同名变量，则会提示重复声明变量：

```tsx
let age: number
// Cannot redeclare block-scoped variable 'age'
```

其实问题出在了变量命名空间，如果不把文件当作模块使用的话 TypeScript 会认为所有文件里的代码都是在同一个作用域里的，所以即使在不同文件也不能声明同名变量。
