import { defineConfig } from 'vitepress'
import { SearchPlugin } from "vitepress-plugin-search";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base: "/ctl/vitepress/",
  title: "代码林",
  head: [["link", { rel: "icon", href: "http://127.0.0.1:9005/ctl/vitepress/favicon.ico" }]],
  description: "A VitePress Site",

  
  // 禁用主题切换按钮
  appearance: false,

  // vite: {
  //   plugins: [SearchPlugin()]
  // },
  themeConfig: {
    outlineTitle: '目录',
    // search: {
    //   provider: 'local'
    // },
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: '首页', link: '/' },
      { text: '常用', link: '/common/common' },
      {
        text: '前端',
        items: [
          {
            // 该部分的标题
            text: 'TypeScript',
            items: [
              { text: 'TypeScript1', link: '/TypeScript/index' },
            ]
          },
          {
            // 该部分的标题
            text: 'js',
            items: [
              { text: 'js', link: '/TypeScript/index', },
            ],

          },
        ]
      },
      {
        text: '后端',
        items: [
          {
            // 该部分的标题
            text: 'javaEE',
            items: [
              { text: '零碎', link: '/JavaPiecemeal/piecemeal' },
              { text: '注解', link: '/java/annotation' },
            ]
          },
          {
            // 该部分的标题
            text: 'javaWeb',
            items: [
              { text: 'js', link: '/java/index', },
            ],

          },
          {
            // 该部分的标题
            text: 'mysql',
            items: [
              { text: 'js', link: '/java/index', },
            ],
            
          },
          {
            // 该部分的标题
            text: 'mybatis',
            items: [
              { text: 'js', link: '/java/index', },
            ],
            
          },
          {
            // 该部分的标题
            text: 'spring',
            items: [
              { text: 'js', link: '/java/index', },
            ],
            
          },
          {
            // 该部分的标题
            text: 'spring Boot',
            items: [
              { text: 'js', link: '/java/index', },
            ],
            
          },
        ]
      },
      { text: 'TypeScript', link: '/TypeScript/index' },
      { text: 'Examples', link: '/markdown-examples' },

    ],

    sidebar: {
      '/common/common':[
        {
          text:'',
          collapsed: false,
          items: [
            {
              text:'',
            },
            {
              text:'',
            }
          ]
        }
      ],
      '/TypeScript/':[
        {
          text:'TypeScript',
          collapsed: false,
          items: [
            {
              text:'什么是TypeScript',link:'/TypeScript/index',
            },
            {
              text:'使用TypeScript',link:'TypeScript/A_TypeScript',
            }
          ]
        }
      ],
      '/JavaPiecemeal/':[
        {
          text:'开始12n',
          collapsed: false,
          items: [
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
            {
              text:'零碎',link:'/JavaPiecemeal/piecemeal',
            },
            {
              text:'序列化',link:'/JavaPiecemeal/Serializable',
            },
          ]
        }
      ]
    },

    // socialLinks: [
    //   { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    // ]
  }
}
)
