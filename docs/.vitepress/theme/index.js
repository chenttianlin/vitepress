// .vitepress/theme/index.js
import DefaultTheme from 'vitepress/theme'
import tag from '../../template/tag.vue'
import alert from '../../template/alert/index';
import './custom.css'

/** @type {import('vitepress').Theme} */
export default {
  extends: DefaultTheme,
  enhanceApp({ app }) {
    // 注册自定义全局组件
    app.component('tag',tag)
    app.component('alert',alert)
  }
}